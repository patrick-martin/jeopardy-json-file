const express = require('express') //express is our HTTP srver -- THIS IS OUR SERVER
const app = express() //-- we get our instance, stored in variable app
const fetch = require('node-fetch') //top of file generally -- CLIENT server
    //const axios = require('axios') //should be at top of file 
const fs = require('fs') //also at top of file-- now we include these modules in our project(dependencies)

const categoryCount = 6
const port = 3000

const getRandomIndex = (length) => Math.floor(Math.random() * length)


async function getRandomCategories() {
    const response = await fetch('https://jservice.kenzie.academy/api/categories') //we can use await/asynch to get Promises instead of .then() methods
    const { categories } = await response.json() //destructuring the category data--because the array we want is inside a property within that object (WE KNOW THIS! computers.clues[0.question-- our hydrated object])
        //console.log(categories.categories) //our array is actually inside an the categories property in the categories object-- weird.
    const selectedCategories = []

    for (let i = 0; i < categoryCount; i++) {
        const index = getRandomIndex(categories.length) //applying function to grab random
        selectedCategories.push(categories[index])
    }


    fs.writeFileSync('categories.json', JSON.stringify(selectedCategories))
    console.log('Categories.json saved!')

}

getRandomCategories()

// axios.get('https://jservice.kenzie.academy/api/categories')
//     .then(response => {
//         fs.writeFileSync('categories.json', JSON.stringify(response.data))
//         console.log('Categories.json saved!')
//     })

//THIS IS OUR SERVER STUFF:
//We are offering 'get' requests at that URL; callback sends a response with the selectedCategories object
//app.listen() initiates the server-- begins to allow 'requests'

app.get('/api/categories', (request, response) => response.send(JSON.stringify(selectedCategories))) //HERE we are opening up our server at this '/api/categories' URL-- we are accepting 'get' requests, where we are sending a response as our JSON object
app.get('/', (request, response) => response.send('hello world'))

app.listen(3000, () => console.info('jService server is running on port ' + port))